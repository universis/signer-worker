import { HttpMethodNotAllowedError } from '@themost/common';
import express from 'express';
import { createProxyMiddleware } from 'http-proxy-middleware';

export function signerWorkerRouter() {
    const router = express.Router();
    router.use('/sign', (req, res, next) => {
        return next(new HttpMethodNotAllowedError('This method is not supported when using remote services.'))
    });
    router.use(createProxyMiddleware({
        target: 'http://localhost:2465/',
        pathRewrite: {
            '^/api/signer' : ''
        },
        changeOrigin: true
    }));
    return router;
}