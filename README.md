# @universis/signer-worker

Universis signer worker for server applications

## Getting started

Install `@universis/signer-worker`

    npm i @universis/signer-worker

Important note: Verify that [Universis Signer](https://gitlab.com/universis/universis-signer) is already installed as aservice. Follow [installation instructions](https://gitlab.com/universis/universis-signer#installation) to install and configure Universis Signer.

## Configuration

Configure [Universis api server](https://gitlab.com/universis/universis-api) to use `SignerWorker` service. Register `@universis/signer-worker#SignerWorker` under universis api server services section of application configuration:

    {
        "services": [
            ...,
            {
                "serviceType": "@universis/signer-worker#SignerWorker"
            }
        ]
    ...
    }
